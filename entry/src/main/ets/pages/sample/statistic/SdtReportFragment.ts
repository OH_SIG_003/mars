/*
* Tencent is pleased to support the open source community by making Mars available.
* Copyright (C) 2016 THL A29 Limited, a Tencent company. All rights reserved.
*
* Licensed under the MIT License (the "License"); you may not use this file except in
* compliance with the License. You may obtain a copy of the License at
* http://opensource.org/licenses/MIT
*
* Unless required by applicable law or agreed to in writing, software distributed under the License is
* distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
* either express or implied. See the License for the specific language governing permissions and
* limitations under the License.
*/

import TableRow from './StatisticsAdapter';
import { SdtLogic } from '@ohos/mars';
import { SignalDetectResult } from '@ohos/mars';

class SdtReportFragment {
  public static detectTypes: string[] = ["PingCheck", "DnsCheck", "NewDnsCheck", "TcpCheck", "HttpCheck"];
  public static tableTitles: string[] = ["detectType", "detail"];
  private table: TableRow[];

  public static initData(sdtHistorys: SignalDetectResult[]): string[] {
    let sdtHistoryTable:string[]=[]
    for (let index = 0; index < sdtHistorys.length; index++) {
      const sdtHistory = sdtHistorys[index];
      let detail = sdtHistory.details;
      let value = "";
      switch (detail.detectType) {
        case SdtLogic.NetCheckType.kPingCheck:
          value = "ping ip:" + detail.detectIP + ", rtt:" + detail.rttStr + ", lossrate:" + detail.pingLossRate;
          break;
        case SdtLogic.NetCheckType.kTcpCheck:
          value = "tcp ip:" + detail.detectIP + ", rtt:" + detail.rttStr + ", errcode:" + detail.errorCode;
          break;
        case SdtLogic.NetCheckType.kHttpCheck:
          value = "http ip:" + detail.detectIP + ", status:" + detail.httpStatusCode + ", errcode:" + detail.errorCode;
          break;
        case SdtLogic.NetCheckType.kDnsCheck:
          value = "dns host:" + detail.dnsDomain + ", ip1:" + detail.dnsIP1 + ", ip2:" + detail.dnsIP2;
          break;
        case SdtLogic.NetCheckType.kNewDnsCheck:
        default:
          value = "";
          break;
      }
      let values: string = [this.detectTypes[detail.detectType], value].toString();
      sdtHistoryTable.push(values)
    }

    return sdtHistoryTable;
  }
}

export default SdtReportFragment;