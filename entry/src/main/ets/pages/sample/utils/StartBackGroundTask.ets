/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import backgroundTaskManager from '@ohos.resourceschedule.backgroundTaskManager';
import WantAgent from '@ohos.app.ability.wantAgent';
import { BusinessError } from '@ohos.base';

export default class StartBackGroundTask {
  private isStopTask: boolean = false;
  private taskId: number = 0;


  startBackgroundTask(){
    let wantAgentInfo: WantAgent.WantAgentInfo = {
      wants: [
        {
          bundleName: "cn.openharmony.mars",
          abilityName: "EntryAbility"
        }
      ],
      actionType: WantAgent.OperationType.START_ABILITY,
      requestCode: 0,
      wantAgentFlags: [WantAgent.WantAgentFlags.UPDATE_PRESENT_FLAG]
    };
    try {
      WantAgent.getWantAgent(wantAgentInfo).then((wantAgentObj) => {
        try {
          backgroundTaskManager.startBackgroundRunning(getContext(this),
            backgroundTaskManager.BackgroundMode.DATA_TRANSFER, wantAgentObj).then(() => {
            console.info("Operation startBackgroundRunning succeeded");
          }).catch((error: BusinessError) => {
            console.error(`Operation startBackgroundRunning failed. code is ${error.code} message is ${error.message}`);
          });
        } catch (error) {
          console.error(`Operation startBackgroundRunning failed. code is ${(error as BusinessError).code} message is ${(error as BusinessError).message}`);
        }
      });
    } catch (error) {
      console.error(`Operation getWantAgent failed. code is ${(error as BusinessError).code} message is ${(error as BusinessError).message}`);
    }
  }

  startTask() {
    this.isStopTask = false;
    this.taskId = setInterval(() => {
      try {
        console.info("StartBackGroundTask startTask go isStopTask "+this.isStopTask);
        if(this.isStopTask){
          clearInterval(this.taskId);
          return;
        }
        this.startBackgroundTask()
      } catch (e) {
        console.info("StartBackGroundTask startTask err " + e);
      }
    }, 5000)
  }

  stopTask() {
    try {
      this.isStopTask = true;
      clearInterval(this.taskId);
    } catch (e) {
      console.info("StartBackGroundTask stopTask err " + e);
    }
  }
}