/*
* Tencent is pleased to support the open source community by making Mars available.
* Copyright (C) 2016 THL A29 Limited, a Tencent company. All rights reserved.
*
* Licensed under the MIT License (the "License"); you may not use this file except in
* compliance with the License. You may obtain a copy of the License at
* http://opensource.org/licenses/MIT
*
* Unless required by applicable law or agreed to in writing, software distributed under the License is
* distributed on an "AS IS" basis, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND,
* either express or implied. See the License for the specific language governing permissions and
* limitations under the License.
*/

/**
 * 一个消息的Bean.
 *
 * @author way
 */
export class ChatMsgEntity {
  private topic: string = ''; //消息群组
  private name: string = ''; //消息来自
  private date: string = ''; //消息日期
  private message: string = ''; //消息内容
  private isComMeg: boolean = true; // 是否为收到的消息

  public gettTopic(): string {
    return this.topic;
  }

  public setTopic(topic: string): void {
    this.topic = topic;
  }

  public getName(): string {
    return this.name;
  }

  public setName(name: string): void {
    this.name = name;
  }

  public getDate(): string {
    return this.date;
  }

  public setDate(date: string): void {
    this.date = date;
  }

  public getMessage(): string {
    return this.message;
  }

  public setMessage(message: string): void {
    this.message = message;
  }

  public getMsgType(): boolean {
    return this.isComMeg;
  }

  public setMsgType(isComMsg: boolean): void {
    this.isComMeg = isComMsg;
  }

  public getChatMsgEntity(): void {
    console.info('ChatMsgEntity this.topic =' + this.topic + '+this.name = ' + this.name + 'this.date = ' + this.date + "+ this.message  = " + this.message + "+ this.isComMeg = " + this.isComMeg)
  }

  constructor(name?: string, date?: string, text?: string, isComMsg?: boolean) {
    this.name = name;
    this.date = date;
    this.message = text;
    this.isComMeg = isComMsg;
  }
}
