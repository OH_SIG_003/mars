/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium';
import { GlobalContext, Log, Xlog } from '@ohos/mars';
import AbilityDelegatorRegistry from '@ohos.app.ability.abilityDelegatorRegistry';

export default function XLogTest() {
  let fileDir: string = '';
  let marsLog: string = '';
  describe('xLogTest', () => {


    // Defines a test suite. Two parameters are supported: test suite name and test suite function.
    beforeAll(() => {
      fileDir = GlobalContext.getContext().getValue('filesDir') as string
      marsLog = fileDir + '/marsLog'
      // Presets an action, which is performed only once before all test cases of the test suite start.
      // This API supports only one parameter: preset action function.
    })
    beforeEach(() => {
      // Presets an action, which is performed before each unit test case starts.
      // The number of execution times is the same as the number of test cases defined by **it**.
      // This API supports only one parameter: preset action function.
    })
    afterEach(() => {
      // Presets a clear action, which is performed after each unit test case ends.
      // The number of execution times is the same as the number of test cases defined by **it**.
      // This API supports only one parameter: clear action function.
    })
    afterAll(() => {
      // Presets a clear action, which is performed after all test cases of the test suite end.
      // This API supports only one parameter: clear action function.
    })

    it('XLogConfig2', 0, () => {
      let xLogConfig: Xlog.XLogConfig = new Xlog.XLogConfig(0, 0, '', "prefix", "", 1, 1, '/Log', 0,);
      expect(JSON.stringify(xLogConfig))
        .assertEqual('{"level":0,"mode":0,"logdir":"","nameprefix":"prefix","pubkey":"","compressmode":1,"compresslevel":1,"cachedir":"/Log","cachedays":0}');
    })

    it('XLogConfig3', 0, () => {
      let xLogConfig: Xlog.XLogConfig = new Xlog.XLogConfig(9, 90, '', "", "", 1, 0, '/marsLog', 16,);
      expect(JSON.stringify(xLogConfig))
        .assertEqual('{"level":9,"mode":90,"logdir":"","nameprefix":"","pubkey":"","compressmode":1,"compresslevel":0,"cachedir":"/marsLog","cachedays":16}');
    })

    it('XLogConfig4', 0, () => {
      let xLogConfig: Xlog.XLogConfig = new Xlog.XLogConfig(1, 1, '', "", "", 1, 7, '', 7,);
      expect(JSON.stringify(xLogConfig))
        .assertEqual('{"level":1,"mode":1,"logdir":"","nameprefix":"","pubkey":"","compressmode":1,"compresslevel":7,"cachedir":"","cachedays":7}');
    })

    it('logV2', 0, () => {
      let xLog = new Xlog();
      let logV = xLog.logV(0, '', 'str', '', 0, 0, 0, 0, '');
      expect(logV).assertUndefined();
    })

    it('logV3', 0, () => {
      let xLog = new Xlog();
      let logV = xLog.logV(-69, '', 'logdir', '', 20, 0, 34, 0, '34');
      expect(logV).assertUndefined();
    })

    it('logV4', 0, () => {
      let xLog = new Xlog();
      let logV = xLog.logV(-90, '', 'logdir', '', 0, 320, 110, 0, '');
      expect(logV).assertUndefined();
    })

    it('logD2', 0, () => {
      let xLog = new Xlog();
      let logD = xLog.logD(0, '', 'str', '', 0, 0, 0, 0, '');
      expect(logD).assertUndefined();
    })

    it('logD3', 0, () => {
      let xLog = new Xlog();
      let logD = xLog.logD(-69, '', 'logdir', '', 20, 0, 34, 0, '34');
      expect(logD).assertUndefined();
    })

    it('logD4', 0, () => {
      let xLog = new Xlog();
      let logD = xLog.logD(-90, '', 'logdir', '', 0, 320, 110, 0, '');
      expect(logD).assertUndefined();
    })

    it('logE2', 0, () => {
      let xLog = new Xlog();
      let logE = xLog.logE(-300, '', '', '', 1, 1, 1, 0, '');
      expect(logE).assertUndefined();
    })

    it('logE3', 0, () => {
      let xLog = new Xlog();
      let logE = xLog.logE(-890, '', '', '', 1, 1, 1, 0, '');
      expect(logE).assertUndefined();
    })

    it('logE4', 0, () => {
      let xLog = new Xlog();
      let logE = xLog.logE(-10, '', '', 'fun', 1, 1, 1, 0, 'error');
      expect(logE).assertUndefined();
    })

    it('logF2', 0, () => {
      let xLog = new Xlog();
      let logF = xLog.logF(0, '', 'files', 'fun56', 13, 9, 1, 1, '');
      expect(logF).assertUndefined();
    })

    it('logI1', 0, () => {
      let xLog = new Xlog();
      let logI = xLog.logI(0, 'string', 'files', '', 0, 0, 1, 1, 'error');
      expect(logI).assertUndefined();
    })

    it('logI2', 0, () => {
      let xLog = new Xlog();
      let logI = xLog.logI(-90, '', '', '', 0, 0, 1, 1, '');
      expect(logI).assertUndefined();
    })

    it('logW2', 0, () => {
      let xLog = new Xlog();
      let logW = xLog.logW(-5, '', 'files', '', 0, 0, 10, 1, '');
      expect(logW).assertUndefined();
    })

    it('logW3', 0, () => {
      let xLog = new Xlog();
      let logW = xLog.logW(0, '', '', '', -1, 0, 1, 1, '');
      expect(logW).assertUndefined();
    })

    it('logW4', 0, () => {
      let xLog = new Xlog();
      let logW = xLog.logW(-115, '', 'files', 'str', 0, 20, 10, 1, '111');
      expect(logW).assertUndefined();
    })

    it('xLog_appenderOpen2', 0, () => {
      let xLog = new Xlog();
      let context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
      let logPath = context.filesDir + "/marsLog/test";
      let appenderOpen = xLog.appenderOpen(1, 2, '', logPath,"LOGSAMPLE",0);
      expect(appenderOpen).assertUndefined();
    })


    it('xLog_appenderOpen3', 0, () => {
      let xLog = new Xlog();
      let context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
      let logPath = context.filesDir + "/marsLog/test";
      let appenderOpen = xLog.appenderOpen(122, 2, '', logPath,"LOGSAMPLE",0);
      expect(appenderOpen).assertUndefined();
    })


    it('xLog_appenderOpen4', 0, () => {
      let xLog = new Xlog();
      let context = AbilityDelegatorRegistry.getAbilityDelegator().getAppContext();
      let logPath = context.filesDir + "/marsLog/test";
      let appenderOpen = xLog.appenderOpen(-101, 12, 'catch', logPath,"LOGSAMPLE",0);
      expect(appenderOpen).assertUndefined();
    })

    it('Xlog_logWrite2', 0, () => {
      let logWrite2 = Xlog.logWrite2(0, '', '', '', 2, 4, 15, 1, '');
      expect(logWrite2).assertUndefined();
    })

    it('Xlog_native_logWrite2', 0, () => {
      let native_logWrite2: ESObject = Xlog.native_logWrite2(0, 0, 'tag', '', '', 2, 1, 1, 0, '');
      expect(native_logWrite2).assertEqual(0);
    })

    it('Xlog_getLogLevel2', 0, () => {
      let xLog = new Xlog();
      xLog.setLogLevel(100)
      let getLogLevel: ESObject = xLog.getLogLevel(-100);
      expect(getLogLevel).assertDeepEquals(6);
    })

    it('Xlog_openLogInstance2', 0, () => {
      let xLog = new Xlog();
      let openLogInstance = xLog.openLogInstance(1, 3, marsLog, marsLog, 'prefix', 0);
      expect(openLogInstance).assertDeepEquals(0);
    })

    it('Xlog_openLogInstance3', 0, () => {
      let xLog = new Xlog();
      let openLogInstance = xLog.openLogInstance(5, 56, marsLog, marsLog, 'prefix', 10);
      expect(openLogInstance).assertDeepEquals(0);
    })

    it('Xlog_openLogInstance4', 0, () => {
      let xLog = new Xlog();
      let openLogInstance = xLog.openLogInstance(11, 3, marsLog, marsLog, 'catch', 100);
      expect(openLogInstance).assertDeepEquals(0);
    })

    it('Xlog_newXlogInstance1', 0, () => {
      let xLog = new Xlog();
      let logConfig: Xlog.XLogConfig = new Xlog.XLogConfig();
      let newXlogInstance: ESObject = xLog.newXlogInstance(logConfig);
      expect(newXlogInstance).assertDeepEquals(-3);
    })

    it('Xlog_newXlogInstance2', 0, () => {
      let xLog = new Xlog();
      let newXlogInstance: ESObject = xLog.newXlogInstance(null);
      expect(newXlogInstance).assertDeepEquals(-3);
    })

    it('Log_appenderFlushSync', 0, () => {
      let appenderFlushSync = Log.appenderFlushSync(true);
      expect(appenderFlushSync).assertUndefined();
    })

    it('Log_getLogLevel', 0, () => {
      let getLogLevel = Log.getLogLevel();
      expect(getLogLevel).assertEqual(6);
    })

    it('Log_setLevel1', 0, () => {
      let setLevel = Log.setLevel(0, true);
      expect(setLevel).assertUndefined();
    })

    it('Log_setLevel2', 0, () => {
      let setLevel = Log.setLevel(2, false);
      expect(setLevel).assertUndefined();
    })

    it('Log_setLevel3', 0, () => {
      let setLevel = Log.setLevel(-100, true);
      expect(setLevel).assertUndefined();
    })

    it('Log_setLevel4', 0, () => {
      let setLevel = Log.setLevel(-30, false);
      expect(setLevel).assertUndefined();
    })

    it('Log_setConsoleLogOpen1', 0, () => {
      let setConsoleLogOpen = Log.setConsoleLogOpen(true);
      expect(setConsoleLogOpen).assertUndefined();
    })

    it('Log_setConsoleLogOpen2', 0, () => {
      let setConsoleLogOpen = Log.setConsoleLogOpen(false);
      expect(setConsoleLogOpen).assertUndefined();
    })

    it('Log_f', 0, () => {
      let f = Log.f('tab', '');
      expect(f).assertUndefined();
    })

    it('Log_e', 0, () => {
      let e = Log.e('tab', '');
      expect(e).assertUndefined();
    })

    it('Log_w', 0, () => {
      let w = Log.w('tab', '');
      expect(w).assertUndefined();
    })

    it('Log_i', 0, () => {
      let i = Log.i('tag', '');
      expect(i).assertUndefined();
    })

    it('Log_d', 0, () => {
      let d = Log.d('tab', '');
      expect(d).assertUndefined();
    })

    it('Log_v', 0, () => {
      let v = Log.v('', '');
      expect(v).assertUndefined();
    })

    it('Log_fFunction2', 0, () => {
      let fFunction = Log.fFunction('tab', '', '');
      expect(fFunction).assertUndefined();
    })

    it('Log_eFunction1', 0, () => {
      let eFunction = Log.eFunction('tab', '', null);
      expect(eFunction).assertUndefined();
    })

    it('Log_eFunction2', 0, () => {
      let eFunction = Log.eFunction('tab', '', '');
      expect(eFunction).assertUndefined();
    })

    it('Log_wFunction1', 0, () => {
      let wFunction = Log.wFunction('tab', '', null);
      expect(wFunction).assertUndefined();
    })

    it('Log_wFunction2', 0, () => {
      let wFunction = Log.wFunction('tab', '', '');
      expect(wFunction).assertUndefined();
    })

    it('Log_iFunction1', 0, () => {
      let iFunction = Log.iFunction('tag', '', null);
      expect(iFunction).assertUndefined();
    })

    it('Log_iFunction2', 0, () => {
      let iFunction = Log.iFunction('tag', '', '');
      expect(iFunction).assertUndefined();
    })

    it('Log_dFunction1', 0, () => {
      let dFunction = Log.dFunction('tab', '', null);
      expect(dFunction).assertUndefined();
    })

    it('Log_vFunction1', 0, () => {
      let vFunction = Log.vFunction('', '', null);
      expect(vFunction).assertUndefined();
    })

    it('Log_vFunction2', 0, () => {
      let vFunction = Log.vFunction('str', '', '');
      expect(vFunction).assertUndefined();
    })

    it('Log_printErrStackTrace', 0, () => {
      let printErrStackTrace = Log.printErrStackTrace('tag', new Error, '', '');
      expect(printErrStackTrace).assertUndefined();
    })

    it('Log_getDeviceInfo', 0, () => {
      let getDeviceInfo = Log.getDeviceInfo();
      expect(getDeviceInfo).assertUndefined();
    })

    it('Log_getSysInfo', 0, () => {
      let getSysInfo = Log.getSysInfo();
      expect(getSysInfo).not().assertUndefined();
    })

    it('Log_openLogInstance', 0, () => {
      let openLogInstance = Log.openLogInstance(1, 1, marsLog, marsLog, 'prefix', 0);
      expect(JSON.stringify(openLogInstance)).assertEqual('{"mLogInstancePtr":0,"mPrefix":"prefix"}')
    })

    it('Log_closeLogInstance', 0, () => {
      let closeLogInstance = Log.closeLogInstance('');
      expect(closeLogInstance).assertUndefined();
    })

    it('Log_getLogInstance1', 0, () => {
      let getLogInstance = Log.getLogInstance('');
      expect(getLogInstance).assertNull();
    })

    it('Log_getLogInstance2', 0, () => {
      let getLogInstance = Log.getLogInstance('prefix');
      expect(JSON.stringify(getLogInstance)).assertEqual('{"mLogInstancePtr":0,"mPrefix":"prefix"}');
    })

    it('Log_getLogInstance3', 0, () => {
      let getLogInstance = Log.getLogInstance('1');
      expect(getLogInstance).assertNull();
    })

    it('Log_getLogInstance4', 0, () => {
      let getLogInstance = Log.getLogInstance('CATCH');
      expect(JSON.stringify(getLogInstance)).assertEqual('null');
    })

    it('Log_LogInstance1', 0, () => {
      let LogInstance: Log.LogInstance = new Log.LogInstance(1, 0, marsLog, marsLog, 'prefix', 0);
      expect(JSON.stringify(LogInstance)).assertEqual('{"mLogInstancePtr":0,"mPrefix":"prefix"}');
    })

    it('Log_LogInstance2', 0, () => {
      let LogInstance: Log.LogInstance = new Log.LogInstance(0, 0, marsLog, marsLog, '', 10);
      expect(JSON.stringify(LogInstance)).assertEqual('{"mLogInstancePtr":0,"mPrefix":""}');
    })

    it('Log_LogInstance3', 0, () => {
      let LogInstance: Log.LogInstance = new Log.LogInstance(-19, 10, marsLog, marsLog, 'mPrefix', 20);
      expect(JSON.stringify(LogInstance)).assertEqual('{"mLogInstancePtr":0,"mPrefix":"mPrefix"}');
    })

    it('Log_LogInstance4', 0, () => {
      let LogInstance: Log.LogInstance = new Log.LogInstance(12, 10, marsLog, marsLog, 'tag', 1000);
      expect(JSON.stringify(LogInstance)).assertEqual('{"mLogInstancePtr":0,"mPrefix":"tag"}');
    })

    it('Log_LogInstance_f', 0, () => {
      let LogInstance: Log.LogInstance = new Log.LogInstance(1, 0, 'cache', '', 'prefix', 0);
      let f = LogInstance.f('tag', '', 'str')
      expect(f).assertUndefined();
    })

    it('Log_LogInstance_e', 0, () => {
      let LogInstance: Log.LogInstance = new Log.LogInstance(1, 0, 'cache', '', 'prefix', 0);
      let e = LogInstance.e('tag', '', 'str')
      expect(e).assertUndefined();
    })

    it('Log_LogInstance_w', 0, () => {
      let LogInstance: Log.LogInstance = new Log.LogInstance(1, 0, 'cache', '', 'prefix', 0);
      let w = LogInstance.w('tag', '', 'str')
      expect(w).assertUndefined();
    })

    it('Log_LogInstance_i', 0, () => {
      let LogInstance: Log.LogInstance = new Log.LogInstance(1, 0, 'cache', '', 'prefix', 0);
      let i = LogInstance.i('tag', '', 'str')
      expect(i).assertUndefined();
    })

    it('Log_LogInstance_d', 0, () => {
      let LogInstance: Log.LogInstance = new Log.LogInstance(1, 0, 'cache', '', 'prefix', 0);
      let d = LogInstance.d('tag', '', 'str')
      expect(d).assertUndefined();
    })

    it('Log_LogInstance_v', 0, () => {
      let LogInstance: Log.LogInstance = new Log.LogInstance(1, 0, 'cache', '', 'prefix', 0);
      let v = LogInstance.v('tag', '', 'str')
      expect(v).assertUndefined();
    })

    it('Log_LogInstance_printErrStackTrace', 0, () => {
      let LogInstance: Log.LogInstance = new Log.LogInstance(1, 0, 'cache', '', 'prefix', 0);
      let printErrStackTrace = LogInstance.printErrStackTrace('str', new Error, '', '')
      expect(printErrStackTrace).assertUndefined();
    })

    it('Log_LogInstance_appenderFlush', 0, () => {
      let LogInstance: Log.LogInstance = new Log.LogInstance(1, 0, 'cache', '', 'prefix', 0);
      let appenderFlush = LogInstance.appenderFlush()
      expect(appenderFlush).assertUndefined();
    })

    it('Log_LogInstance_appenderFlushSync', 0, () => {
      let LogInstance: Log.LogInstance = new Log.LogInstance(1, 0, 'cache', '', 'prefix', 0);
      let appenderFlushSync = LogInstance.appenderFlushSync()
      expect(appenderFlushSync).assertUndefined();
    })
  })
}
