/*
 * Copyright (C) 2024 Huawei Device Co., Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

import { describe, beforeAll, beforeEach, afterEach, afterAll, it, expect } from '@ohos/hypium';
import { BaseEvent } from "@ohos/mars";
import netConnection from '@ohos.net.connection';

export default function baseEventTest() {
  describe('ActsBaseEventTest', () => {
    // Defines a test suite. Two parameters are supported: test suite name and test suite function.
    beforeAll(() => {
      // Presets an action, which is performed only once before all test cases of the test suite start.
      // This API supports only one parameter: preset action function.
    })
    beforeEach(() => {
      // Presets an action, which is performed before each unit test case starts.
      // The number of execution times is the same as the number of test cases defined by **it**.
      // This API supports only one parameter: preset action function.
    })
    afterEach(() => {
      // Presets a clear action, which is performed after each unit test case ends.
      // The number of execution times is the same as the number of test cases defined by **it**.
      // This API supports only one parameter: clear action function.
    })
    afterAll(() => {
      // Presets a clear action, which is performed after all test cases of the test suite end.
      // This API supports only one parameter: clear action function.
    })
    it('onCreate', 0, () => {
      expect(BaseEvent.onCreate()).assertEqual(0);
    })

    it('onDestroy', 0, () => {
      expect(BaseEvent.onDestroy()).assertEqual(0);
    })

    it('onNetworkChange', 0, () => {
      expect(BaseEvent.onNetworkChange()).assertEqual(0);
    })

    it('onForeground1', 0, () => {
      expect(BaseEvent.onForeground(true)).assertEqual(0);
    })

    it('onForeground2', 0, () => {
      expect(BaseEvent.onForeground(false)).assertEqual(0);
    })

    it('onSingalCrash', 0, () => {
      expect(BaseEvent.onSingalCrash(0)).assertEqual(0);
    })

    it('onExceptionCrash', 0, () => {
      expect(BaseEvent.onExceptionCrash()).assertEqual(0);
    })

    it('checkConnInfo', 0, () => {
      let NetConnection = netConnection.createNetConnection()
      let value: ESObject
      NetConnection.on('netCapabilitiesChange', (data) => {
        value = BaseEvent.ConnectionReceiver.checkConnInfo(data)
      })
      expect(value).assertUndefined();
    })

    it('isNetworkChange', 0, () => {
      let NetConnection = netConnection.createNetConnection()
      let value: ESObject
      NetConnection.on('netCapabilitiesChange', (data) => {
        value = BaseEvent.ConnectionReceiver.isNetworkChange(data)
      })
      expect(value).assertUndefined();
    })

  })
}