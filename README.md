# Mars

## Overview

Mars is a cross-platform network component that implements persistent and non-persistent connections in network requests. It works on the socket layer and provides better performance in network optimization. Currently, Mars does not support HTTP. It can significantly improve your development efficiency.

## Effect
! [Mars](mars.png)

## Building and Running

To compile the Mars library, you need to add the OpenSSL dependency to **mars\library\src\main\cpp** and [integrate the OpenSSL to the application HAP](https://gitcode.com/openharmony-sig/tpc_c_cplusplus/blob/master/thirdparty/openssl/docs/hap_integrate.md).

1. Enable the support of x86_64 for cross compilation.

2. Change the OpenSSL version number in the HPKBUILD file.

  ```
pkgver=OpenSSL_1_1_1t 
// Change the version number.
pkgver=OpenSSL_1_1_1w
  ```

3. Download [OpenSSL OpenSSL_1_1_1w](https://github.com/openssl/openssl/archive/refs/tags/OpenSSL_1_1_1w.tar.gz), run the following command to obtain the SHA512 value, and replace the content in the SHA512SUM file.

  ```
sha512num openssl-OpenSSL_1_1_1w.tar.gz
  ```

4. Add the **openssl** folder generated after compilation to the **cpp** directory. The **openssl** folder is generated in the **usr** folder in the **lycium** directory.

![img.png](img/img.png)

For Windows and Mac, integrate the patch and then build the project.
1. Download the source code.
````shell
git clone https://gitcode.com/openharmony-sig/mars.git --recurse-submodules
````
2. Run the following commands in sequence to automatically integrate the patch:
````shell
chmod +x automatically_apply_mars_patch_files.sh

./automatically_apply_mars_patch_files.sh

````
The patch installation is complete.
## How to Install

```shell
ohpm install @ohos/mars 
```
For details, see [Installing an OpenHarmony HAR](https://gitcode.com/openharmony-tpc/docs/blob/master/OpenHarmony_har_usage.en.md).

## How to Use

Step 1: Initialize the Mars component and import it to your project.
```mars
...
import { Mars, StnLogic, Xlog, BuildConfig } from '@ohos/Mars/';
import MarsServiceNative from '../wrapper/service/MarsServiceNative';
import MarsServiceProfile from '../wrapper/service/MarsServiceProfile';
import marsnapi from 'libmarsnapi.so';

...

```
Step 2: Call the corresponding APIs through marsnapi.
```mars
marsnapi.BaseEvent_onCreate();
}
```

## Available APIs

* StnLogic.setLonglinkSvrAddr - Sets the DEBUG IP address of a persistent connection.
* StnLogic.StnLogic_setShortlinkSvrAddr - Sets the DEBUG IP address of a non-persistent connection.
* StnLogic.setDebugIP - Sets the DEBUG IP address, regardless of persistent and non-persistent connections.
* StnLogic.startTask - Starts a task.
* StnLogic.stopTask - Stops a task.
* StnLogic.hasTask - Checks whether a task exists.
* StnLogic.redoTask - Redoes all persistent and non-persistent connection tasks. Note that this API will cause a persistent connection to reconnect.
* StnLogic.clearTask - Stops and clears all unfinished tasks.
* StnLogic.reset - Stops and clears all unfinished tasks and performs initialization again.
* StnLogic.resetAndInitEncoderVersion - Stops and clears all unfinished tasks, performs initialization again, and resets the encoder version.
* StnLogic.setBackupIPs - Sets the backup IP address, which is used when both the persistent and non-persistent connections are unavailable.
* StnLogic.makesureLongLinkConnected: Checks the status of the persistent connection. If the connection fails, the system will try to reconnect it.
* StnLogic.setSignallingStrategy - Sets the signaling keepalive policy.
* StnLogic.keepSignalling - Sends a signaling keepalive packet.
* StnLogic.stopSignalling - Stops signaling keepalive.
* StnLogic.setClientVersion - Sets the client version to be added to the proprietary protocol header of the persistent connection.
* StnLogic.getLoadLibraries - Obtains the loaded modules at the bottom layer.
* StnLogic.req2Buf: Obtains the data sent by the upper layer.
* StnLogic.buf2Resp: Sends the received signaling response packet to the upper layer for parsing.
* StnLogic.trafficData - Reports the traffic consumed by signaling.
* SdtLogic.setCallBack - Sets the signaling detection callback instance, which is used to notify the upper layer of the detection result.
* SdtLogic.setHttpNetcheckCGI - Sets a URI for HTTP connectivity detection.
* Xlog.logWrite - Sets log writing mode 1.
* Xlog.logWrite2 - Sets log writing mode 2.
* Xlog.native_logWrite2 - Sets a custom log writing mode.
* Xlog.setLogLevel - Sets the log level.
* Xlog.getLogLevel - Obtains the log level.
* Xlog.newXlogInstance - Creates a single Xlog instance.
* Xlog.releaseXlogInstance - Releases a single Xlog instance.

## Constraints

Mars has been verified in the following versions:

- DevEco Studio: 4.1Canary (4.1.3.414), OpenHarmony SDK: API 11 (4.1.0.55)
- IDE: DevEco Studio NEXT Developer Preview2: 4.1.3.600, OpenHarmony SDK: API11 (4.1.0)

## Directory Structure
```
|---- library  
|     |---- cpp  # Sample code
|           |---- mars # C++ library files
|     |---- ets
|           |---- sdt  
|                 |---- SdtLogic # sdt external APIs
|           |---- stn  
|                 |---- StnLogic # stn external APIs
|           |---- xlog # xlog external APIs
|                 |---- Xlog # xlog external APIs
|---- README.md  # Readme
|---- README_zh.md  # Readme
```


## About obfuscation
- Code obfuscation, please see[Code Obfuscation](https://docs.openharmony.cn/pages/v5.0/zh-cn/application-dev/arkts-utils/source-obfuscation.md)
- If you want the mars library not to be obfuscated during code obfuscation, you need to add corresponding exclusion rules in the obfuscation rule configuration file obfuscation-rules.txt：

```
-keep
./oh_modules/@ohos/mars
```


## How to Contribute
If you find any problem when using Mars, submit an Issue](https://gitcode.com/openharmony-sig/mars/issues) or a [PR](https://gitcode.com/openharmony-sig/mars/pulls).

## License
This project is licensed under the terms of the [MIT License](https://gitcode.com/openharmony-sig/mars/blob/master/LICENSE).
