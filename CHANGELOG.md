## 2.0.3-rc.0
1.Code obfuscation

## 2.0.2
1.Fix 906 IDE running XTS error。
2.Fix the issue of the release mode entering the chat window crashing
3.Chore:Added supported device types


## 2.0.2-rc.1
1.Fix the issue of the release mode entering the chat window crashing

## 2.0.2-rc.0

1.修复906版本IDE运行XTS报错问题。

## 2.0.1

1.修复断网重连问题。
2.修复appfree crash问题。
3.修复消息中断问题
4.修复在任务结束后，onTaskEnd函数没有回调结果的问题。
5.修复建立网络长连接，网络连接状态没有回调的问题。
6.修复library打包失败问题。
7.修复xlog无法写入日志文件问题。
8.修复消息、断网重连不稳定问题。
9.修复xlog文本长度超过1024导致的crash问题。
10.修复xlog控制台不输出日志问题。

## 2.0.1-rc.3

1.修复断网重连问题。
2.修复appfree crash问题。
3.修复消息中断问题

## 2.0.1-rc.2

修复2.0.1-rc.1版本的har包异常，不能正常使用的问题。

## 2.0.1-rc.1

1. 修复在任务结束后，onTaskEnd函数没有回调结果的问题。
1. 适配x86架构

## 2.0.1-rc.0
1. 修复建立网络长连接，网络连接状态没有回调的问题。

## 2.0.0
1. ArkTS新语法适配
2. 适配DevEco Studio 版本：4.1.3.414，OpenHarmony SDK: 4.1.0.55。
3. 原项目是直接上传源码，基于源码封装napi。本次适配删除了cpp/mars目录，将源码以submodules的形式引入，将封装的napi以patch的形式应用，实现与原项目对等的能力。
## 1.0.0

- Mars 实现的功能具体如下：


	marsnapi.BaseEvent_onCreate();
	
	marsnapi.BaseEvent_onInitConfigBeforeOnCreate(packer_encoder_version);
	
	marsnapi.BaseEvent_onDestroy();
	
	marsnapi.BaseEvent_onForeground(true);
	
	marsnapi.BaseEvent_onNetworkChange();
	
	marsnapi.BaseEvent_onSingalCrash(sig);
	
	marsnapi.BaseEvent_onExceptionCrash();
	
	marsnapi.StnLogic_setLonglinkSvrAddr(profile.longLinkHost(), profile.longLinkPorts(), "");
	
	marsnapi.StnLogic_startTask(_task);
	
	marsnapi.StnLogic_stopTask();
	
	marsnapi.StnLogic_hasTask(taskid);
	
	marsnapi.StnLogic_redoTask();
	
	marsnapi.StnLogic_clearTask();
	
	marsnapi.StnLogic_makesureLongLinkConnected();
	
	marsnapi.StnLogic_setSignallingStrategy();
