## mars 单元测试用例

该测试用例基于OpenHarmony系统下，进行单元测试

### 单元测试用例覆盖情况

| 接口名                                  | 是否通过 | 备注  |
|--------------------------------------|------|-----|
| BaseEvent.onCreate()                 | pass |
| BaseEvent.onDestroy()                | pass |
| BaseEvent.onNetworkChange()          | pass |
| BaseEvent.onForeground()             | pass |
| BaseEvent.onSingalCrash()            | pass |
| BaseEvent.onExceptionCrash()         | pass |
| StnLogic.setLonglinkSvrAddr()        | pass |
| StnLogic.setShortlinkSvrAddr()       | pass |
| StnLogic.setDebugIP()                | pass |
| StnLogic.startTask()                 | pass |
| StnLogic.stopTask()                  | pass |
| StnLogic.hasTask()                   | pass |
| StnLogic.setBackupIPs()              | pass |
| StnLogic.makesureLongLinkConnected() | pass |
| StnLogic.setSignallingStrategy()     | pass |
| StnLogic.keepSignalling()            | pass |
| StnLogic.stopSignalling()            | pass |
| StnLogic.setClientVersion()          | pass |
| StnLogic.getLoadLibraries()          | pass |
| StnLogic.genTaskID()                 | pass |
| StnLogic.req2Buf()                   | pass |
| StnLogic.buf2Resp()                  | pass |
| StnLogic.trafficData()               | pass |
| Xlog.XLogConfig()                    | pass |
| Xlog.logV()                          | pass |
| Xlog.logD()                          | pass |
| Xlog.logE()                          | pass |
| Xlog.logF()                          | pass |
| Xlog.logI()                          | pass |
| Xlog.logW()                          | pass |
| Xlog.setAppenderMode()               | pass |
| Xlog.appenderOpen()                  | pass |
| Xlog.appenderClose()                 | pass |
| Xlog.appenderFlush()                 | pass |
| Xlog.logWrite()                      | pass |
| Xlog.logWrite2()                     | pass |
| Xlog.setLogLevel()                   | pass |
| Xlog.getLogLevel()                   | pass |
| Xlog.native_logWrite2()              | pass |
| Xlog.openLogInstance()               | pass |
| Xlog.releaseXlogInstance()           | pass |
| Xlog.newXlogInstance()               | pass |
| Xlog.setConsoleLogOpen()             | pass |
| Xlog.setMaxFileSize()                | pass |
| Xlog.setMaxAliveTime()               | pass |
| Log.setLogImp()                      | pass |
| Log.getImpl()                        | pass |
| Log.appenderOpen()                   | pass |
| Log.appenderFlush()                  | pass |
| Log.appenderFlushSync()              | pass |
| Log.getLogLevel()                    | pass |
| Log.setLevel()                       | pass |
| Log.setConsoleLogOpen()              | pass |
| Log.f()                              | pass |
| Log.e()                              | pass |
| Log.w()                              | pass |
| Log.i()                              | pass |
| Log.d()                              | pass |
| Log.v()                              | pass |
| Log.fFunction()                      | pass |
| Log.eFunction()                      | pass |
| Log.wFunction()                      | pass |
| Log.iFunction()                      | pass |
| Log.dFunction()                      | pass |
| Log.vFunction()                      | pass |
| Log.printErrStackTrace()             | pass |
| Log.getDeviceInfo()                  | pass |
| Log.getSysInfo()                     | pass |
| Log.openLogInstance()                | pass |
| Log.closeLogInstance()               | pass |
| Log.getLogInstance()                 | pass |

